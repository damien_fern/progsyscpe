#include <stdio.h> 
#include <string.h>

int main(int argc, char *argv[]) { 
	int i, j;
	char* chaines = *argv;
    int nbArg = argc;
    int somme = 0;
    float moyenne = 0;

    printf("-----------------------------------\n");
    if(nbArg > 1)
    {
		for (i = 1; i < nbArg; ++i)
		{
			if(verif(argv[i]) == 0)
			{				
				return 0;
			}
			int nombre = atoi(argv[i]);
			somme += nombre;
		}

		moyenne = (float) somme / (float)(nbArg - 1);
		printf("Moyenne : %.2f\n", moyenne);
    }
    else
    {
    	printf("Aucune moyenne à calculer\n");
    }
    printf("-----------------------------------\n");
    return 0;
}

int verif(char* argument)
{
	int nombre = atoi(argument);
	int resultat;
	if(nombre == 0 && strcmp("0", argument) != 0)
	{
		resultat = 0;
	}
	else
	{
		if(nombre >= 0 && nombre <= 20)
		{
			resultat = 1;
		}
		else
		{
			resultat = 0;
		}
	}

    if(resultat == 0)
    {
		printf("Note non valide : %s pose probleme\n", argument);
    	printf("-----------------------------------\n");
    }
	return resultat;
}