#include <stdio.h> 
#include <string.h>

int main(int argc, char *argv[]) { 
	int i, j;
	char* chaines = *argv;
    int nbArg = argc;
    printf("-----------------------------------\n");
	for (i = 1; i < nbArg; ++i)
	{
		char* chaine = argv[i];
    	int longueurChaine = strlen(chaine);
		for(j = longueurChaine; j > -1 ; j--) {
			printf("%c",chaine[j]);
		}
		printf("\n");
	}
    printf("-----------------------------------\n");
    return 0;
}
