#include <stdio.h>
#include <string.h>

int contains(char* haystack, char* needle) {

	size_t len_needle = strlen(needle);
	int i;	
	for(i = 0 ;  i < len_needle; i++) {
		if (needle[i] != haystack[i]) {
			return 0;
		}
	}

	return 1;
}


int main(int argc, char* argv[]) {

	extern char **environ;
	int i;
	for(i = 0; environ[i] != NULL; i++) {
		if(contains(environ[i], argv[1]) == 1) {
			printf("%s\n", environ[i]);
		}
	}
}
