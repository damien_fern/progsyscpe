#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>


//Affiche la variable d'environnement passée dans le 1er argument
int main (int nbarg, char *arg[]) {
	char* commandes[3][2] = {{"who", NULL}, {"ps", NULL}, {"ls", "-l"}};
	printf("\n");	
	for(int i = 0 ; i < 3 ; i++) {
		if(fork() == 0) {
			execlp(commandes[i][0],commandes[i][0],commandes[i][1], NULL);
			exit(0);
		}
		else
		{
			wait();
		}
	}
	//while(wait(0)!=-1);
	return 0;
}