#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>


int main (int nbarg, char *arg[]) {
	if(nbarg < 2) {
		printf("Veuillez rentrer au moins un nom de fichier .c\n");
		return -1;
	}	
	for(int i = 1 ; i < nbarg ; i++) {
		if(fork() == 0) {
			execlp("gcc","gcc","-std=c99", "-c", arg[i], NULL);
			exit(0);
		}
	}
	while(wait(0)!=-1);

	for(int i = 1 ; i < nbarg ; i++) {
		char *pDot = strrchr(arg[i], 'c');
    	if(pDot!=NULL)
        	*pDot = 'o';
    }

	char **const ArgumentV = malloc(nbarg+4 * sizeof(char *));
	ArgumentV[0] = "gcc";
	ArgumentV[1] = "-std=c99";
	ArgumentV[2] = "-o";
	ArgumentV[3] = "executable";
	ArgumentV[4+nbarg-1] = NULL;
	int index = 1;
	for(int j = 4 ; j < nbarg-1+4 ; j++) {
		ArgumentV[j] = arg[index];
		index++;
	}
	execv("/usr/bin/gcc",ArgumentV);
	printf("echec de la compilation\n");
	return 0;
}