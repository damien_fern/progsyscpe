#include <stdio.h> 
#include <string.h>

int main(int argc, char *argv[]) { 
	for(int k = 0; k < 3; ++k)
	{
		int pid = fork();
		printf("(k = %d) : Je suis le processus : %d, mon père est %d, retour %d\n", k, getpid(), getppid(), pid);
	}
	return 0;
}