#include <stdio.h> 
#include <string.h>

int main(int argc, char *argv[]) { 
	if(fork() == 0)
	{
		execlp("nautilus", "nautilus", NULL);
	}
	else
	{
		execlp("ls", "ls", NULL);
	}
	return 0;
}