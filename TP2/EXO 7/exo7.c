#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int main(int argc, char* argv[]){
	int i;
	int n =atoi(argv[1]);
	int pid;
	int ppid = 0;
	int Etat;
	int pid_fils;
	for (i=0; i<=n; i++){
		if (fork() == 0){
			pid = getpid();
			ppid = getppid();
			sleep(2*i);
			printf("Mon id : %d & l'id de mon père : %d\n",pid,ppid);
			exit(i);
		}
	}
	
	while(1){
		pid_fils = wait(&Etat);
		if(pid_fils == -1) break;
		printf("Mon fils : %d & son état : %d\n",pid_fils,Etat);
	}
	printf("Je n'ai plus de fils et je m'arrête !\n");
	return 0;
}