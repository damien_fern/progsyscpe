#include <stdlib.h>
#include <stdio.h>

int main(int argc, char* argv[]){
	int i, delai;
	for (i=0; i<=4; i++) if (fork()) break;
    srand(getpid());
    delai = rand()%4;
    sleep(delai);
    wait(NULL); // ligne ajouté
    printf("Mon nom est %c, j'ai dormi pendant %d secondes\n", 'A'+i, delai);
	return 0;
}