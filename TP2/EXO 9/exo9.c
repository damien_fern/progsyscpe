#include <stdio.h>
#define N 3

main(int argc, char const *argv[])
{
    int i, pid1, pid2;
    for(i=0;i<N;i++)
    {
        fork();
        fork();
        wait();
        wait();
    }
    return 0;
}
