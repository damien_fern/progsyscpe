# Exeo 6

## 1

On se trouve dans le père
PID = 0 pour le fils

## 2

Oui, programme déterministe, car le wait permet l'attente de la fin des processus fils pour exécuter le processus père.

## 3 

Non car tous les processus sont lancés simultanément.

## 4

08642

## 5

Oui si l'OS le bloque (overflow).
