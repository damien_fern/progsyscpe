#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include <ctype.h>
#define NBTHREAD 2


pthread_t th[NBTHREAD];
int idefix; // index
pthread_mutex_t lock; 

void majuscule(char* file){
  FILE* fp1, *fp2;
  int c = 0;
  fp1=fopen(file, "r"); //r pour lecture
  fp2=fopen(file, "r+");//r+ pour lecture et écriture+
  if ((fp1==NULL) || (fp2==NULL)){
    perror("fopen");exit(1);
  }
  while (c!=EOF){
    c=fgetc(fp1);
    if(c!=EOF){
      fputc(toupper(c),fp2); //Passer chacun des caractères de c en majuscule
    }
  }
  
  fclose(fp1);
  fclose(fp2);
}

void* handleFileMutex(void* arg)
{
  char** argv=arg;
  while(idefix > 1)
  {
    pthread_mutex_lock(&lock);
    majuscule(argv[idefix]);
    idefix--;
    pthread_mutex_unlock(&lock);
  }

  return NULL;
}


int main (int argc, char* argv[]){

  int i;
  idefix = argc-1;
  if(pthread_mutex_init(&lock, NULL) != 0)
  {
    printf("Error Mutex");
    return 1;
  }

  for (i=1; i <= NBTHREAD;i++){
    pthread_create(&th[i-1], NULL, handleFileMutex, argv);
  }
  
  for (i=1; i <= NBTHREAD;i++){
    
    pthread_join(th[i-1], NULL);
    
  }
  return 0;
}
