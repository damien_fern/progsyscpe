#include <stdio.h>
#include <stdlib.h>
#include <pthread.h> 

// Variables globales
volatile long double pi = 0.0;
pthread_mutex_t lock;

long double nbIntervals; // Nombre d'intervalles
int nbTh; // Nombre de threads

void* calcul(void* thId)
{
    long double x, larg, somme = 0;
    int i, threadID = *((int*)thId);
    larg = 1.0 / nbIntervals; // Largeur de l'intervalle 

    for(i = threadID ; i < nbIntervals; i += nbTh) 
    {
        x = (i+0.5) * larg;
        somme += 4.0 / (1.0 + x*x);
    }

    somme *= larg; 
    pthread_mutex_lock(&lock);
    pi += somme;
    pthread_mutex_unlock(&lock); 
    return NULL;
} 

int main(int argc, char **argv)
{
    pthread_t *threads;
    int *threadID;
    int i;
    
    
    if (argc == 3) 
    {
      nbIntervals = atoi(argv[1]); 
      nbTh = atoi(argv[2]); 
      threads = malloc(nbTh*sizeof(pthread_t));
      threadID = malloc(nbTh*sizeof(int));
      pthread_mutex_init(&lock, NULL);

      for (i = 0; i < nbTh; i++)
      {
        threadID[i] = i;
        pthread_create(&threads[i], NULL, calcul, threadID+i);
      }

      for (i = 0; i < nbTh; i++)
      {
         pthread_join(threads[i], NULL);
      }
      printf("Estimation %32.30Lf \n", pi);
      printf("Vraie valeur : 3.141592653589793238462643383279...\n");
    }
    else
    {
      printf("Usage: ./main <nbIntervals> <nbThreads>");    
    }

    return 0;
}
