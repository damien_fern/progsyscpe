#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

void sigint_exit(int n) {
	printf("Réception d'un signal SIGINT\n");
	exit(0);
}

int main(int argc, char** argv) {

	signal(SIGINT, sigint_exit);

	printf("My PID is : %d\n", getpid());	
	printf("En éxecution ...\n");
	
	for(;;) {
	}
	return 0;
} 
