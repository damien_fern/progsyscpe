#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {

	signal(SIGINT, SIG_IGN);

	printf("My PID is : %d\n", getpid());	
	printf("En éxecution ...\n");
	
	for(;;) {
	}
	return 0;
} 
