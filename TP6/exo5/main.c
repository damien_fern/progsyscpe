#include <stdio.h>
#include <signal.h>
#include <stdlib.h>

void handle_sigint(int n) {

	printf("Interception d'un signal SIGINT\n");

}

// On remarque que le signal SIGINT est intercepté par le fils, et que 
// le fils ne s'arrête donc plus quand on utilise CTRL+C. Le père
// s'arrête de fonctionner après 3 itérations, mais le fils continuera
// tant qu'aucun signal SIGKILL ne lui sera envoyé par une commande KILL


int main(int argc, char** argv) {

	pid_t forked_pid = fork();
	if(!forked_pid) {
		// Processus fils
		signal(SIGINT, handle_sigint);
		int i = 0;
		for(i = 0 ; i >= 0; i++) {
			sleep(1);
			printf("Affichage fils %d\n", i+1);
		}
	}
	else {
		// Processus père
		int i = 0;
		for(i = 0 ; i < 3 ; i++) {
			sleep(1);
			printf("Affichage père %d\n", i+1);
		}
	}

	return 0;
} 
