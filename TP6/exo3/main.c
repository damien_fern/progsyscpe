#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

int fin;

void sigint_exit(int n) {
	fin = 1;
}

int main(int argc, char** argv) {

	fin = 0;
	signal(SIGINT, sigint_exit);

	printf("My PID is : %d\n", getpid());	

	for(;;) {

		if(fin) {
			printf("En éxecution ...\n");
		}

	}
	return 0;
} 
