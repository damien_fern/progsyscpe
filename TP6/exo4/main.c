#include <stdio.h>
#include <signal.h>
#include <stdlib.h>

int main(int argc, char** argv) {

	pid_t forked_pid = fork();
	if(!forked_pid) {
		// Processus fils
		int i = 0;
		for(i = 0 ; i >= 0; i++) {
			sleep(1);
			printf("Affichage fils %d\n", i+1);
		}
	}
	else {
		// Processus père
		int i = 0;
		for(i = 0 ; i < 3 ; i++) {
			sleep(1);
			printf("Affichage père %d\n", i+1);
		}
		kill(forked_pid,SIGKILL);
	}

	return 0;
} 
