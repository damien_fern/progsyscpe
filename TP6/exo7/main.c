#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

void parent_handle_sigusr(int n) {
	// SIGUSR1: Signal de timeout
	if(n == SIGUSR1) {
		printf("Trop tard !!\n");
		exit(0);
	}
}

void child_handle_sigusr(int n) {
}


int main(int argc, char** argv) {

	pid_t parent_pid = getpid();
	pid_t child_pid = fork();
	if(!child_pid) {
		// Processus fils 
		// Gère le time-out : attends 5 secondes 
		// et envoie un SIGUSR1 pour signale le timeout au père
		sleep(5);
		kill(parent_pid, SIGUSR1);
		exit(0);
	} else {
		// Processus père
		// Callback pour le timeout
		signal(SIGUSR1, parent_handle_sigusr);
	} 

	// Le père seulement ira ici	
	int ok = 0;
	while(!ok) {
		int val = 0;
		printf("Svp un entier: \n");
		// vérifie qu'on a bien entré un entier, si c'est le cas 
		// on sort de la boucle
		if(scanf("%d",&val) != 1)
		{
			while(getchar() != '\n');
		} else {
			ok = 1;		
		}
	}

	// Si on est ici c'est qu'on a rentré un entier à temps
	printf("Ok merci!!\n");
	// ... donc, on tue le fils
	kill(child_pid, SIGKILL);

	return 0;
}
