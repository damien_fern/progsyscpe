#include <stdio.h>
#include <signal.h>
#include <stdlib.h>

void handle_sigusr(int n) {
	if(n == SIGUSR1) {
		printf("Affichage fils\n");
	} else if(n == SIGUSR2) {
		exit(0);
	}
}

int main(int argc, char** argv) {

	pid_t forked_pid = fork();
	if(!forked_pid) {
		// Processus fils
		signal(SIGUSR1, handle_sigusr);
		signal(SIGUSR2, handle_sigusr);		

		// boucle infinie
		for(;;) {
		}
	}
	else {
		// Processus père
		int i = 0;
		// Nombre arbitraire de boucles (c'est pas spécifié dans l'exercice ...)
		for(i = 0 ; i < 8 ; i++) {
			// Envoie un signal SIGUSR1 sur les itérations 3 et 5
			if(i == 3 || i == 5) {
				kill(forked_pid, SIGUSR1);		
			}

			sleep(1);
			printf("Affichage père %d\n", i+1);
		}
		// Envoie un signal SIGUSR2 pour signaler la fin du traitement
		kill(forked_pid, SIGUSR2);
	}

	return 0;
} 
