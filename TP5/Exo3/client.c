#include <stdio.h>
#include "lib.h"

int main(int argc, char const *argv[])
{
    printf("Tube crée\n");
    int tube = creer_tube();
    char* message;
    printf("Before Lecture\n");
    message = lecture();
    printf("After Lecture\n");

    detruire_tube();
    printf("Tube détruit\n");
    return 0;
}
