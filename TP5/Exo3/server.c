#include <stdio.h>
#include "lib.h"
#include <regex.h>

struct requete_client_serveur {
    int clientPid; // PID du client
    char expression[20]; // chaine de caractère à évaluer 
};

int main(int argc, char const *argv[])
{
    printf("Tube crée\n");
    int tube = creer_tube();
    char message[50];
    printf("Before Lecture\n");
    lecture(message);
    printf("After Lecture\n");
    printf("Message : %s\n", message);

    regex_t regex;
    char* regexRule = "([0-9]*)(\+|\-)([0-9]*)";
    regmatch_t pmatch[1];
    size_t maxGroups = 3;
    regmatch_t groupArray[maxGroups];

    if (regcomp(&regex, regexRule, REG_ICASE|REG_EXTENDED) != 0){
        printf("regcomp error\n");
        exit(1);
    }
    if (regexec(&regex, message, maxGroups, groupArray, 0) == 0)
    {
      unsigned int g = 0;
      for (g = 0; g < maxGroups; g++)
        {
          if (groupArray[g].rm_so == (size_t)-1)
            break;  // No more groups

          char sourceCopy[strlen(message) + 1];
          strcpy(sourceCopy, message);
          sourceCopy[groupArray[g].rm_eo] = 0;
          printf("Group %u: [%2u-%2u]: %s\n",
                 g, groupArray[g].rm_so, groupArray[g].rm_eo,
                 sourceCopy + groupArray[g].rm_so);
        }
    }
    detruire_tube();
    regfree(&regex);
    printf("Tube détruit\n");
    return 0;
}
