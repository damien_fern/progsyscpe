#include <sys/fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int creer_tube()
{
    return mkfifo("TubeNomme", S_IRWXU | S_IRWXG | S_IRWXO);
}

void detruire_tube()
{
    unlink("TubeNomme");
}

void lecture(char* data)
{
    int tube, longueur;
    printf("Open tube\n");
    tube=open("TubeNomme", O_RDONLY);
    printf("Tube Opened\n");
    longueur = read(tube, data, 30);
    data[longueur] = '\0';
}

void ecriture()
{
    int tube;
    char message[50];
    sprintf(message, "processus %d", getpid());
    tube=open("TubeNomme", O_WRONLY);
    write(tube, message, strlen(message));
    close(tube);
}
