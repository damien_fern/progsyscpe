#include <stdio.h>
#include "exo2.h"

int main(int argc, char const *argv[])
{
    printf("Tube crée\n");
    int tube = creer_tube();
    printf("Before Lecture\n");
    lecture();
    printf("After Lecture\n");

    detruire_tube();
    printf("Tube détruit\n");
    return 0;
}