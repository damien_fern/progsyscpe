#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "../semaphore/cpe_sem.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define N 10

// Retourne l'index de la valeur minimale du tableau
int mini(int table[]) {
	int i = 0, min = 10000000, saved_i = 0;
	for(i = 0 ; i < N ;i++) {
		if(table[i] < min) {
			min = table[i];
			saved_i = i;
		}
	}

	return saved_i;
}

void display(int table[]) {
	int i = 0;
	for(i = 0 ; i < N ; i++) {
		printf("%d\n", table[i]);
	}	
	printf("\n");
}

int main(int argc, char** argv) {

	// Génère les valeurs de p2
	int valeurs_process[N] = {
		1,2,3,4,5,6,7,8,9,10
	};

	// Récupère le segment de mémoire partagée
	key_t shm_key = 1;
	int shm_id = shmget(shm_key, sizeof(int) * 3, 0);
	if (shm_id < 0) {
		perror("Erreur avec le shmget");
		exit(1);
	}

	// Map le segment partagée dans la mémoire du processus
	int* valeurs = shmat(shm_id, NULL, 0);

	// Récupère le sémaphore d'accès pour p0  
	int sem_lock_p0 = sem_create(1,0);
	int sem_lock_p1 = sem_create(2,0);
	int sem_lock_p2 = sem_create(3,0);

	int j; 
	for(j = 0 ; j < N ; j++) {
		// Ecrit dans la mémoire
		int i = mini(valeurs_process);
		valeurs[1] = valeurs_process[i];

		// Delock p0 
		V(sem_lock_p0);

		// Lock en attendant que p0 écrive les bonnes valeurs
		P(sem_lock_p2);

		// Vérifie les changements dans la mémoire partagée 
		if(valeurs[2] == -1) {
			break;
		}

		valeurs_process[i] = valeurs[1];

		V(sem_lock_p1);
		P(sem_lock_p2);
	}

	printf("Valeurs p2: \n");
	display(valeurs_process);

	return 0;
}

