#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "../semaphore/cpe_sem.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define N 10

void echange(int table[], int i, int j) 
{
	int backup = table[i];
	table[i] = table[j];
	table[j] = backup;
}

int main(int argc, char** argv) 
{

	int s1 = sem_create(1, 0);
	int s2 = sem_create(2, 0);
	int s3 = sem_create(3, 0);

	key_t shm_key = 1;
	int shm_id = 0;

	shm_id = shmget(shm_key, sizeof(int) * 3, IPC_CREAT | SHM_W | SHM_R);
	if(shm_id < 0)
	{
		perror("Erreur avec le shmget");
		exit(1);
	}

	int* valeurs;
	valeurs = shmat(shm_id, NULL, 0);

	int i;
	for(i = 0 ; i < N ; i++) 
	{
		P(s1);
		P(s1);

		valeurs[2] = 0;
		if(valeurs[0] > valeurs[1]) 
		{
			echange(valeurs, 0, 1);
		} 
		else 
		{
			valeurs[2] = -1;
		}
		
		V(s2);
		V(s3);

		if(valeurs[2] == -1) 
		{ 
			break; 
		}
	}

	shmctl(shm_id, IPC_RMID, NULL);
	sem_delete(s1);
	sem_delete(s2);
	sem_delete(s3);

	return 0;
}

