#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "../semaphore/cpe_sem.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define N 10

int maxi(int table[]) 
{
	int i = 0, max = -10000000, saved_i = 0;
	for(i = 0 ; i < N ;i++) 
	{
		if(table[i] > max) {
			max = table[i];
			saved_i = i;
		}
	}

	return saved_i;
}

void affichage(int table[]) 
{
	int i = 0;
	for(i = 0 ; i < N ; i++) 
	{
		printf("%d\n", table[i]);
	}
	printf("\n");
}

int main(int argc, char** argv) 
{

	int val_proc[N] = 
	{
		1,2,3,4,5,6,7,8,9,10
	};

	key_t shm_key = 1;
	int shm_id = shmget(shm_key, sizeof(int) * 3, 0);
	if (shm_id < 0) 
	{
		perror("Erreur avec le shmget");
		exit(1);
	}

	int* values = shmat(shm_id, NULL, 0);

	int s1 = sem_create(1,0);
	int s2 = sem_create(2,0);
	int s3 = sem_create(3,0);
	
	int j; 
	for(j = 0 ; j < N ; j++) 
	{
		int i = maxi(val_proc);
		values[0] = val_proc[i];

		V(s1);

		P(s2);

		if(values[2] == -1) 
		{
			break; 
		}
		val_proc[i] = values[0];

		V(s3);
		P(s2);
	}

	printf("Valeurs de p1: \n");
	affichage(val_proc);
	return 0;
}

