#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../semaphore/cpe_sem.h"

int main(int argc, char** argv) {

	int sem = sem_create(0, 1);
	sem_delete(sem);

	return 0;
} 
