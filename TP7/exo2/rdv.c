#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../semaphore/cpe_sem.h"

int main(int argc, char** argv) {

	if(argc != 2) {
		printf("Usage: ./rdv <id> avec ID >= 1\n");
		return 1;
	}

	int process = atoi(argv[1]);

	int my_sem = sem_create(process,0);
	sleep(3);
	printf("Le process %d est au RDV\n", process);
	if(process-1 > 0) {
		int wait_sem = sem_create(process-1, 0);
		P(wait_sem);
	}
	V(my_sem);	

	printf("Process %d terminé\n", process);
	return 0;
}
