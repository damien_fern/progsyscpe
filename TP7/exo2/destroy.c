#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../semaphore/cpe_sem.h"

int main(int argc, char** argv) {

	if(argc != 2) {
		printf("Usage: ./destroy <id>\n");
		return 1;	
	}
	int process = atoi(argv[0]);
	int sem = sem_create(process, 0);
	sem_delete(sem);

	return 0;
}
