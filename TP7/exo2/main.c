#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../semaphore/cpe_sem.h"

void tache_1() {
	// Attends, pour qu'on puisse vérifier qu'on attende
	// bien le sémpahore

	sleep(2);
	printf("Tâche 1\n");
}

void tache_2() {
	printf("Tâche 2\n");
}

int main(int argc, char** argv) {

	int sem = sem_create(0, 1);
	P(sem);

	if(!fork()) {
		// Processus fils 
		P(sem);
		tache_2();
		sem_delete(sem);
	} else {
		tache_1();
		// Lâche le lock sur le sémaphore
		V(sem);
	}


	return 0;
} 
