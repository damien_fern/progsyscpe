#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../semaphore/cpe_sem.h"

int main(int argc, char** argv) {

	if(argc != 2) {
		printf("Usage: <id>\n");
	}

	int id = atoi(argv[1]);
	int sem = sem_create(id, 0);

	return 0;
}
