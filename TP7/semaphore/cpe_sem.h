#include <sys/sem.h>

int sem_create(key_t cle, int initval);
void sem_delete(int semid);
void P(int semid);
void V(int semid);
