#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include "../semaphore/cpe_sem.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define MAX_SERVER 3

struct ServerInfo {
	unsigned int next_id;
	pid_t pids[MAX_SERVER];
} ServerInfo;


// uint permettant de sauvegarder l'ID de ce server
unsigned int self_id;

void handle_sigusr1(int sig) {
	char str[25];
	sprintf(str, "Le serveur %d s'arrête", self_id); 
	execlp("echo", "echo", str, NULL);	
}

int main(int argc, char** argv) {

	signal(SIGUSR1, handle_sigusr1);
	// Récupère un segment de mémoire partagée
	key_t shm_key = 1;
	int shm_id = 0;

	shm_id = shmget(shm_key, sizeof(ServerInfo), IPC_CREAT | SHM_W | SHM_R);
	if(shm_id < 0) {
		perror("shmget error");
		exit(1);
	}

	// Map le segment partagé dans la mémoire du processus
	struct ServerInfo* servinfo;       
	servinfo = shmat(shm_id, NULL, 0);

	// Récupère le sémaphore d'accès pour le buffer
	int sem_lock_buffer = sem_create(1,1);

	P(sem_lock_buffer);
	// Si on a trouvé aucune info sur le serveur
	if(servinfo == NULL) {
		// Crée le serveur dans la mémoire partagée 
		servinfo = malloc(sizeof(ServerInfo));
		servinfo->next_id = 1;
		servinfo->pids[0] = getpid(); 	
		self_id = 0;
	} else {
		// Si on en a trouvé, alors on sauvegarde et on inscrit notre PID dans la liste 
		self_id = servinfo->next_id++;
		servinfo->pids[self_id] = getpid();
	}

	// Si on est le dernier serveur, il faut envoyer un signal à tous les autres serveurs pour
	// leur dire de sortir de leur pause
	if(servinfo->next_id == MAX_SERVER) {
		int i;
		for(i = 0; i < MAX_SERVER-1 ; i++) {
			pid_t target_pid = servinfo->pids[i];
			kill(target_pid, SIGUSR1);
		}

		// Détruit les ressources d'IPC
		// Sémaphore
		sem_delete(sem_lock_buffer);
		// shm
		shmctl(shm_id, IPC_RMID, NULL);

		// Remplace ce process par la commande cmd associée 
		handle_sigusr1(SIGUSR1);
	} else {

		// Lâche la sémaphore pour que les autres serveurs arrêtent de bloquer
		V(sem_lock_buffer);
		// Met en pause le programme. Il se reveillera avec l'arrivée d'un signal (SIGUSR1).
		// Evite l'attente active
		pause();
	} 	


	return 0;
}
