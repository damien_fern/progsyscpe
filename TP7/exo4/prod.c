#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "../semaphore/cpe_sem.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define TAILLE_BUFFER 100

int main(int argc, char** argv)
{
	if( argc != 2 )
	{
		printf("Usage: prod <char>\n");
		exit(1);
	}
	if( strlen(argv[1]) >= 2 )
	{
		printf("Un char, pas une chaine de char\n");
		exit(1);
	}

	key_t shm_key = 1;
	int shm_id = shmget(shm_key, TAILLE_BUFFER, 0666);
	if (shm_id < 0) 
	{
		perror("Erreur avec le shmget");
		exit(1);
	}

	char* buffer = shmat(shm_id, NULL, 0);
	int s1 = sem_create(1,1);

	while(1)
	{

		P(s1);
		if(strlen(buffer) < TAILLE_BUFFER) 
		{
			strcat(buffer, argv[1]);	
			V(s1);
			break;
		} 
		V(s1);

	}


}
