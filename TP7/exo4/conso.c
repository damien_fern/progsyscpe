#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "../semaphore/cpe_sem.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define TAILLE_BUFFER 100

int main(int argc, char** argv) 
{

	key_t shm_key = 1;
	int shm_id = shmget(shm_key, TAILLE_BUFFER, 0666);
	if (shm_id < 0)
	{
		perror("Erreur avec le shmget");
		exit(1);
	}

	char* buffer = shmat(shm_id, NULL, 0);

	int s1 = sem_create(1,1);

	while(1) 
	{
		P(s1);
		if(strlen(buffer) != 0) 
		{

			char char_extract = buffer[0];
			printf("Char: %c\n", char_extract);
			if(buffer[0] == '\0') 
			{
				return;
			}
			char new_char = buffer[1];
			int i = 0;
			while(new_char != '\0') 
			{
				buffer[i] = new_char;
				i++;
				new_char = buffer[i+1];
			}
			buffer[i] = '\0';
			V(s1);
			break;
		} 

		V(s1);
	}
}
