#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../semaphore/cpe_sem.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define TAILLE_BUFFER 100

int main(int argc, char** argv) 
{

	key_t shm_key = 1;
	int shm_id = shmget(shm_key, TAILLE_BUFFER, 0666);

	if(shm_id < 0) 
	{
		perror("Erreur avec le shmget");
		exit(1);
	}

	shmctl(shm_id, IPC_RMID, NULL);
	int sem = sem_create(1,1);
	sem_delete(sem);
	return 0;
}
