#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../semaphore/cpe_sem.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

int main(int argc, char** argv) 
{

	size_t TAILLE_BUFFER = 100;
	key_t shm_key = 1;
	int shm_id = 0;

	shm_id = shmget(shm_key, TAILLE_BUFFER, IPC_CREAT | SHM_W | SHM_R ); 
	if(shm_id < 0)
	{
		perror("Erreur avec le shmget");
		exit(1);
	}

	int i = sem_create(1,1);

	return 0;
}
