#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../semaphore/cpe_sem.h"

int main(int argc, char** argv) {

	printf("Lancé un récepteur\n");

	int sem_emetteur = sem_create(2,0);
	int sem_recepteur = sem_create(1,0);

	V(sem_recepteur);
	P(sem_emetteur);
	printf("Récepteur tué");

	return 0;
}
