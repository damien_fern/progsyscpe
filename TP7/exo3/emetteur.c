#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../semaphore/cpe_sem.h"

int main(int argc, char** argv) {

	int sem_lock_recepteur = sem_create(3,0);
	P(sem_lock_recepteur);
	
	if(argc != 2) {
		printf("Usage: ./emetteur <message>\n");
	}

	printf("Emetteur lancé: %s\n", argv[1]);

	int sem_recepteur = sem_create(1,0);
	int sem_emetteur = sem_create(2,0);
	P(sem_recepteur);
	P(sem_recepteur);
	V(sem_emetteur);
	V(sem_emetteur);

	V(sem_lock_recepteur);

	return 0;
}
