#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../semaphore/cpe_sem.h"

int main(int argc, char** argv) {

	int sem = sem_create(1,0);
	int sem_two = sem_create(2,0);
	int sem_three = sem_create(3,1);
	sem_delete(sem);
	sem_delete(sem_two);
	sem_delete(sem_three);


	return 0;
}
