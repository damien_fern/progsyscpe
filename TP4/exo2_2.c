#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char** argv) {

	if(argc != 4) {
		printf("usage: <fichier> <chaine> <sortie>\n");
		return 1;
	}	


	int* first_pipe = malloc(sizeof(int) * 2);
	pipe(first_pipe);

	if(!fork()) {

		int* second_pipe = malloc(sizeof(int) * 2);
		pipe(second_pipe);

		// Process fils
		// Processus grep chaine | tail -n 5 > sortie
		if(!fork()) {
			// Process petit-fils
			// tail -n 5 > sortie
			close(first_pipe[0]);
			close(first_pipe[1]);
			close(second_pipe[1]);

			int file_out = creat(argv[3], 0644);
			dup2(second_pipe[0], 0);
			dup2(file_out, 1);			
			close(second_pipe[0]);
			close(file_out);	
			execlp("tail", "tail", "-n", "5", NULL);
			exit(0);
		}

		// grep chaine
		close(first_pipe[1]);
		close(second_pipe[0]);

		dup2(first_pipe[0], 0);
		close(first_pipe[0]);

		dup2(second_pipe[1], 1);
		close(second_pipe[1]);		
		
		execlp("grep", "grep", argv[2], NULL);
	} else {
		// Process père
		// Processus sort < fichier
		int file_in = open(argv[1], 0644);
		close(first_pipe[0]);	
		dup2(file_in, 0);
		dup2(first_pipe[1], 1);	
		close(first_pipe[1]);
		close(file_in);
		execlp("sort", "sort", NULL);
	}


	return 0;
}
