#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

void generator(int n, int even, int odd, int sum_even_pipe, int sum_odd_pipe) {
	srand(time(NULL));
	int i;
	for(i = 0 ; i < n ; i++) {
		int aleatoire = rand() % 100;
		printf("Valeur: %d\n", aleatoire);
		if(aleatoire % 2 == 0) {
			write(even, &aleatoire, sizeof(int));
		} else {
			write(odd, &aleatoire, sizeof(int));
		}
	}

	int val = -1;
	write(even, &val, sizeof(int));
	write(odd, &val, sizeof(int));

	int sum_even = 0;
	int sum_odd = 0;

	read(sum_even_pipe, &sum_even, sizeof(int));	
	read(sum_odd_pipe, &sum_odd, sizeof(int));	

	printf("somme pair: %d\n", sum_even);
	printf("somme impair: %d\n", sum_odd);
}

char* read_input(int pipe) {
	char* buf = malloc(sizeof(char) * 2);
	read(pipe, buf, 2);
	return buf;
}

void filtre(int pipe, int pipe_sum) {
	int sum = 0;
	int val = 1;
	while(val != -1) {
		read(pipe, &val, sizeof(int));
		if(val != -1) {
			sum += val;	
		}
	} 
	
	write(pipe_sum, &sum, sizeof(int)); 
}


int main(int argc, char** argv) {

	if(argc != 2) {
		printf("usage: ./main <nombre de nombres à générer>\n");
		return 1;
	}
	int n = atoi(argv[1]);

	int* pipe_odd = malloc(sizeof(int) * 2);
	pipe(pipe_odd);

	int* pipe_even = malloc(sizeof(int) * 2);
	pipe(pipe_even);

	int* sum_odd = malloc(sizeof(int) * 2);
	pipe(sum_odd);

	int* sum_even = malloc(sizeof(int) * 2);
	pipe(sum_even);

	if(!fork()) {
		filtre(pipe_even[0], sum_even[1]);	
	} else if(!fork()) {
		filtre(pipe_odd[0], sum_odd[1]);
	}
	else {
		generator(n, pipe_even[1], pipe_odd[1], sum_even[0], sum_odd[0]);
	}


	close(pipe_odd[0]);
	close(pipe_odd[1]);
	close(pipe_even[0]);
	close(pipe_even[1]);
	close(sum_odd[0]);
	close(sum_odd[1]);
	close(sum_even[0]);
	close(sum_even[1]);

	return 0;
}

